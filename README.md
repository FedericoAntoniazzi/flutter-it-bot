# Flutter Italia Bot
Official telegram bot for the italian Flutter community on Telegram.

## Commands
```
/start - Start the bot
/help - Show the list of available commands
/bonk - Bonk a user. Must be a reply of a message
/mybonk - Show your bonks stats
/excalibonk - 10% of probability to bonk a user 5 times
/leaderboard - Show the leaderboard of bonked users
```

## Running the bot
### Requirements
* A service account for accessing Firebase Firestore
* A bot token from t.me/botfather

### Run as a python script
1. Clone the project
2. Run with the following command
```bash
python3 -m bot
```

### Run as a container with Docker/Podman
The project supports containers and have an official multiarch image:
`registry.gitlab.com/federicoantoniazzi/flutter-it-bot`.
It supports `linux/amd64` and `linux/arm64` so can be run also in a Rasperry Pi.

You can either use the CLI or a `docker-compose.yml` file:
```bash
docker run --rm \
	--name flutteritbot \
	-v ./service-account.json:/bot/service-account.json \
	--env TOKEN="MYBOT:TOKEN" \
	registry.gitlab.com/federicoantoniazzi/flutter-it-bot
```

```yaml
version: '3'

services:
  bot:
    image: registry.gitlab.com/federicoantoniazzi/flutter-it-bot:v1.0.2
    container_name: flutteritbot
    restart: always
    environment:
      - TOKEN="MYBOT:TOKEN"
    volumes:
      - ./service-account.json:/bot/service-account.json:ro
```
