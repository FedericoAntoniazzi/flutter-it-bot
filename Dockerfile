FROM python:3.10-slim

LABEL maintainer="Federico Antoniazzi"
LABEL description="Telegram bot for the Flutter Italia Community"

ENV TOKEN ""
ENV SERVICE_ACCOUNT_PATH "/bot/service-account.json"

# RUN useradd flutteritbot
RUN adduser flutteritbot

ADD Pipfile* ./
RUN pip3 install pipenv && \
	pipenv install --system --deploy --ignore-pipfile && \
	pip3 uninstall pipenv -y

WORKDIR /
COPY --chown=flutteritbot:flutteritbot bot/ /bot

USER flutteritbot
ENTRYPOINT python -m bot
