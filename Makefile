.PHONY: dev
dev:
	pipenv run npx nodemon --exec python3 -m bot -e py,yml

.PHONY: lint
lint:
	black bot
	flake8 --config .flake8
