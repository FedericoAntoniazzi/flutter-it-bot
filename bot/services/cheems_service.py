import logging
import os

from google.cloud import firestore
from google.oauth2 import service_account

logger = logging.getLogger()


class CheemsService:
    def __init__(self):
        credentials = service_account.Credentials.from_service_account_file(
            os.environ.get("SERVICE_ACCOUNT_PATH", "./service-account.json")
        )
        self._store = firestore.Client(credentials=credentials)
        self._collection = self._store.collection("cheems")

    def bonks_count_of(self, user_id: int) -> int:
        doc = self._collection.document(str(user_id)).get()

        if not doc.exists:
            return 0

        return doc.to_dict().get("bonks", 0)

    def bonk_user(self, user_id: int, username: str, bonks: int = 1) -> int:
        doc = self._collection.document(str(user_id))
        current_bonks = self.bonks_count_of(user_id)
        doc.set({"bonks": current_bonks + bonks, "username": username})
        return current_bonks + bonks

    def top_users(self, count: int = 5):
        docs = (
            self._collection.order_by("bonks", direction="DESCENDING")
            .limit(count)
            .stream()
        )
        return [
            dict(
                user_id=doc.id,
                username=doc.to_dict().get("username", ""),
                bonks=doc.to_dict().get("bonks", 0),
            )
            for doc in docs
        ]
