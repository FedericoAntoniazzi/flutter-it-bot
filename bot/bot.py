import logging
from telegram import Update, constants
from telegram.ext import (
    Application,
    ChatMemberHandler,
    CallbackQueryHandler,
    CommandHandler,
    Defaults,
    MessageHandler,
    filters,
)
import re
import bot.handlers as handlers

logger = logging.getLogger()


class FlutterItaliaBot:
    def __init__(
        self,
        token: str,
    ):
        # Configure default options for the bot
        defaults: Defaults = Defaults(
            parse_mode=constants.ParseMode.MARKDOWN_V2, disable_web_page_preview=True
        )
        self.__application: Application = (
            Application.builder().token(token).defaults(defaults).build()
        )

    def setup(self):
        logger.info("Start configuring bot handlers")

        # Core
        self.__application.add_handler(CommandHandler("start", handlers.start))
        self.__application.add_handler(CommandHandler("help", handlers.help))

        # Funny stuff
        self.__application.add_handler(
            ChatMemberHandler(handlers.greet_new_user, ChatMemberHandler.CHAT_MEMBER)
        )
        self.__application.add_handler(
            CommandHandler("mybonks", handlers.mybonks, filters.ChatType.GROUPS)
        )
        self.__application.add_handler(
            CommandHandler("bonk", handlers.bonk, filters.ChatType.GROUPS)
        )
        self.__application.add_handler(
            CommandHandler("excalibonk", handlers.excalibonk, filters.ChatType.GROUPS)
        )
        self.__application.add_handler(
            CommandHandler("leaderboard", handlers.leaderboard, filters.ChatType.GROUPS)
        )
        self.__application.add_handler(
            MessageHandler(
                filters.Regex(re.compile(r"\b(dart:)?dio\b", re.IGNORECASE))
                & filters.ChatType.GROUPS
                & ~filters.COMMAND,
                handlers.pope,
            )
        )

        # Admin tools
        self.__application.add_handler(
            CommandHandler(
                "controlpanel", handlers.controlpanel, filters.ChatType.GROUPS
            )
        )
        self.__application.add_handler(
            CallbackQueryHandler(handlers.controlpanel_action)
        )

        logger.info("Bot handlers configured successfully")

    def run(self):
        logger.info("Starting application")
        self.__application.run_polling(allowed_updates=Update.ALL_TYPES)
        logger.info("Application execution terminated")
