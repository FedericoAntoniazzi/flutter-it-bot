import logging
import os

from google.cloud import firestore
from google.oauth2 import service_account
from typing import Dict

logger = logging.getLogger()


class GroupSettings:
    def __init__(self, group_id: int):
        credentials = service_account.Credentials.from_service_account_file(
            os.environ.get("SERVICE_ACCOUNT_PATH", "./service-account.json")
        )
        self._store = firestore.Client(credentials=credentials)
        self._doc_ref = self._store.collection("settings").document(str(group_id))

    @property
    def pope(self) -> bool:
        doc = self._doc_ref.get().to_dict()
        return doc.get("dio", {}).get("enabled", False)

    @pope.setter
    def pope(self, value: bool):
        doc = self._doc_ref.get().to_dict()
        doc["dio"] = dict(enabled=value)
        self._doc_ref.set(doc)

    @property
    def bonk(self) -> bool:
        doc = self._doc_ref.get().to_dict()
        return doc.get("bonk", {}).get("enabled", False)

    @bonk.setter
    def bonk(self, value: bool):
        doc = self._doc_ref.get().to_dict()
        doc["bonk"] = dict(enabled=value)
        self._doc_ref.set(doc)


class GroupSettingsManager:
    _settings: Dict[int, GroupSettings] = {}

    def get_settings_of(self, group_id: int) -> GroupSettings:
        if group_id not in self._settings.keys():
            self._settings[group_id] = GroupSettings(group_id)

        return self._settings[group_id]
