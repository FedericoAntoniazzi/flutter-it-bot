import i18n
from telegram import Update
from telegram.constants import ChatType
from telegram.ext import ContextTypes
from bot.settings import Gsm


async def help(update: Update, _: ContextTypes.DEFAULT_TYPE) -> None:
    """Explain bot commands"""
    help_message = [i18n.t("bot.help.title"), i18n.t("bot.help.base")]

    if update.effective_chat.type in [ChatType.GROUP, ChatType.SUPERGROUP]:
        gsm = Gsm.get_settings_of(update.effective_chat.id)
        if gsm.bonk:
            help_message.append(i18n.t("bot.help.bonk"))

    await update.message.reply_markdown_v2("\n".join(help_message))
