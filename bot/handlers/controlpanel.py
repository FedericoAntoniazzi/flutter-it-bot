import logging
import i18n
from bot.filters import allow_admins_only
from bot.settings import Gsm, GroupSettings
from telegram import Update, InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import ContextTypes

logger = logging.getLogger()


def controlpanel_keyboard(group_id: int):
    group_settings: GroupSettings = Gsm.get_settings_of(group_id)

    keyboard = []
    for setting in ["pope", "bonk"]:

        if setting == "pope":
            emoji = "enabled" if group_settings.pope else "disabled"
        elif setting == "bonk":
            emoji = "enabled" if group_settings.bonk else "disabled"

        keyboard.append(
            [
                InlineKeyboardButton(
                    i18n.t(f"bot.controlpanel.{emoji}", setting=setting),
                    callback_data=setting,
                )
            ]
        )

    return keyboard


@allow_admins_only
async def controlpanel(update: Update, _: ContextTypes.DEFAULT_TYPE) -> None:
    group_id = update.effective_chat.id
    logger.info(f"Received control command from {group_id}")

    reply_markup = InlineKeyboardMarkup(controlpanel_keyboard(group_id))
    await update.message.reply_text(
        i18n.t("bot.controlpanel.title"), reply_markup=reply_markup
    )


@allow_admins_only
async def controlpanel_action(update: Update, _: ContextTypes.DEFAULT_TYPE) -> None:
    query = update.callback_query
    await query.answer()

    group_id = update.effective_chat.id
    group_name = update.effective_chat.title
    group_settings: GroupSettings = Gsm.get_settings_of(group_id)

    if query.data == "pope":
        group_settings.pope = not group_settings.pope
        logger.info(
            f"Dio status changed in {group_name}. Current status: {group_settings.pope}"
        )
    elif query.data == "bonk":
        group_settings.bonk = not group_settings.bonk
        logger.info(
            f"Bonk status changed in {group_name}. Current status: {group_settings.bonk}"
        )

    reply_markup = InlineKeyboardMarkup(controlpanel_keyboard(group_id))
    await query.edit_message_reply_markup(reply_markup=reply_markup)
