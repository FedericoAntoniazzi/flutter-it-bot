import logging
import i18n
from telegram import Update, ChatMember, ChatMemberUpdated
from telegram.ext import ContextTypes
from typing import Optional, Tuple

logger = logging.getLogger()


def extract_status_change(
    chat_member_update: ChatMemberUpdated,
) -> Optional[Tuple[bool, bool]]:
    """Takes a ChatMemberUpdated instance and extracts whether the 'old_chat_member' was a member of the chat
    and whether the 'new_chat_member' is a member of the chat. Returns None, if the status didn't change.
    """
    status_change = chat_member_update.difference().get("status")
    old_is_member, new_is_member = chat_member_update.difference().get(
        "is_member", (None, None)
    )

    if status_change is None:
        return None

    old_status, new_status = status_change
    was_member = old_status in [
        ChatMember.MEMBER,
        ChatMember.OWNER,
        ChatMember.ADMINISTRATOR,
    ] or (old_status == ChatMember.RESTRICTED and old_is_member is True)
    is_member = new_status in [
        ChatMember.MEMBER,
        ChatMember.OWNER,
        ChatMember.ADMINISTRATOR,
    ] or (new_status == ChatMember.RESTRICTED and new_is_member is True)

    return was_member, is_member


async def greet_new_user(update: Update, _: ContextTypes.DEFAULT_TYPE) -> None:
    """Greet the user that has just joined the group"""
    user_name = update.chat_member.new_chat_member.user
    chat_title = update.effective_chat.title or i18n.t("default.chat_title")

    result = extract_status_change(update.chat_member)
    _, is_member = result
    # Send message only when users join the chat
    if is_member:
        logger.info(
            "New user joined the chat", dict(user=user_name.name, chat=chat_title)
        )

        await update.effective_chat.send_message(
            text=i18n.t(
                "bot.greet", username=user_name.mention_markdown(), chat_name=chat_title
            )
        )
