import i18n
import logging
import secrets
from bot.services import Cheems
from bot.settings import Gsm, GroupSettings
from telegram import Update, Message
from telegram.ext import ContextTypes
from telegram.helpers import mention_markdown

logger = logging.getLogger()


def bonk_enabled(func):
    async def inner(u: Update, c: ContextTypes.DEFAULT_TYPE):
        group_id = u.effective_chat.id
        group_name = u.effective_chat.title
        group_settings: GroupSettings = Gsm.get_settings_of(group_id)

        if not group_settings.bonk:
            logger.info(f"Cannot invoke bonk in {group_name} because it's disabled")
            return

        return await func(u, c)

    return inner


def validate_bonk(func):
    async def inner(u: Update, c: ContextTypes.DEFAULT_TYPE):
        reply: Message = u.effective_message.reply_to_message

        if reply is None:
            await u.message.reply_text(i18n.t("bot.cheems.bonk-usage"))
            return None

        if u.effective_message.from_user.id == reply.from_user.id:
            await u.message.reply_text(i18n.t("bot.cheems.cannot-bonk-self-user"))
            return None

        if c.bot.id == reply.from_user.id:
            await u.message.reply_text(i18n.t("bot.cheems.cannot-bonk-self-bot"))
            return None

        return await func(u, c)

    return inner


@bonk_enabled
async def mybonks(update: Update, _: ContextTypes.DEFAULT_TYPE) -> None:
    user = update.effective_user
    bonks = Cheems.bonks_count_of(user.id)
    await update.message.reply_text(i18n.t("bot.cheems.mybonks", bonks=bonks))


@bonk_enabled
@validate_bonk
async def bonk(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    reply: Message = update.effective_message.reply_to_message
    user_to_bonk = reply.from_user

    username = (
        user_to_bonk.username
        if user_to_bonk.username is not None
        else user_to_bonk.full_name
    )
    user_bonks_count = Cheems.bonk_user(user_to_bonk.id, username)
    logger.info(f"User {username} got bonked. Current count: {user_bonks_count}")

    mention = mention_markdown(user_to_bonk.id, username, version=2)
    await update.message.reply_markdown_v2(
        i18n.t("bot.cheems.bonk", user_mention=mention, count=user_bonks_count)
    )


@bonk_enabled
@validate_bonk
async def excalibonk(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    reply: Message = update.effective_message.reply_to_message
    user_to_bonk = reply.from_user

    username = (
        user_to_bonk.username
        if user_to_bonk.username is not None
        else user_to_bonk.full_name
    )

    # Set the probability to 10%
    random_number = secrets.randbelow(100)
    probability = 10

    logger.info(f"Excalibonk: number={random_number} probability={probability}")

    if random_number > probability:
        await update.message.reply_markdown(i18n.t("bot.cheems.excalibonk-failed"))
        return

    user_bonks_count = Cheems.bonk_user(user_to_bonk.id, username, 5)
    logger.info(f"User {username} got excalibonked. Current count: {user_bonks_count}")

    mention = mention_markdown(user_to_bonk.id, username, version=2)
    await update.message.reply_markdown_v2(
        i18n.t("bot.cheems.excalibonk", user_mention=mention, count=user_bonks_count)
    )


@bonk_enabled
async def leaderboard(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    top_users = Cheems.top_users(5)

    board = [i18n.t("bot.cheems.leaderboard.title")]

    for i, user in enumerate(top_users):
        line_template = "bot.cheems.leaderboard.places.nth"

        if i == 0:
            line_template = "bot.cheems.leaderboard.places.first"
        if i == 1:
            line_template = "bot.cheems.leaderboard.places.second"
        if i == 2:
            line_template = "bot.cheems.leaderboard.places.third"

        username = user.get("username") if user.get("username") != "" else "Nobody"
        board.append(
            i18n.t(
                line_template,
                username=username,
                number=i + 1,
                bonks=user.get("bonks", 0),
            )
        )

    await update.message.reply_text("\n".join(board))
