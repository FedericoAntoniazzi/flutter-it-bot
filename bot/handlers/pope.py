import logging
import secrets
from telegram import Update
from telegram.ext import ContextTypes
from bot.settings import Gsm

logger = logging.getLogger()

# Sticker IDs from https://t.me/addstickers/PapaBergoglio
pope_sticker_ids = [
    "CAACAgQAAxkBAAICKWBVD5yKX6hdTdG9Z2va9OwQXDKcAALfAAMrqWsB9WiJsA-I8V0eBA",
    "CAACAgQAAxkBAAICJ2BVD5ohPybaExpm2w0HsTz_d2ygAALdAAMrqWsByFVwOH3jhlkeBA",
    "CAACAgQAAxkBAAICJWBVD5hHdIjVqBfvyUOXJPYx2cqnAALZAAMrqWsBKPZH4R4YgZ8eBA",
]


async def pope(update: Update, _: ContextTypes.DEFAULT_TYPE) -> None:
    enabled = Gsm.get_settings_of(update.effective_chat.id).pope

    if enabled:
        sticker = secrets.choice(pope_sticker_ids)
        await update.message.reply_sticker(sticker=sticker)
