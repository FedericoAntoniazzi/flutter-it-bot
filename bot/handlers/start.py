import logging
import i18n
from telegram import Update
from telegram.ext import ContextTypes

logger = logging.getLogger()


async def start(update: Update, _: ContextTypes.DEFAULT_TYPE) -> None:
    """Send a message when the command /start is issued"""
    user = update.effective_user
    chat_title = update.effective_chat.title or i18n.t("default.chat_title")

    await update.message.reply_markdown_v2(
        text=i18n.t(
            "bot.start", username=user.mention_markdown_v2(), chat_name=chat_title
        )
    )
