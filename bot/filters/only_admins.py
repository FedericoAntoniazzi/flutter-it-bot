import logging

from telegram import Update
from telegram.ext import ContextTypes

logger = logging.getLogger()


def allow_admins_only(func):
    """
    Filter to allow a message only from the admin users of a group

    ATTENTION: This a decorator and not a telegram filter because they are not designed to make API calls
    """

    async def inner(u: Update, c: ContextTypes.DEFAULT_TYPE):
        admins = await u.effective_chat.get_administrators()
        user = u.effective_user

        if any(user.id == admin.user.id for admin in admins):
            return await func(u, c)

        chat = u.effective_chat.title
        logger.info(
            f"A normal user ({user.full_name}) tried to access a feature limited to admins in {chat}"
        )

    return inner
