import logging
import os
import i18n
from bot.bot import FlutterItaliaBot

logging.basicConfig(
    format="%(asctime)s - %(levelname)s - "
    "%(filename)s:%(funcName)s:%(lineno)d "
    "msg='%(msg)s' args='%(args)s' exc_info='%(exc_info)s'",
    level=logging.INFO,
)


def main():
    i18n.set("file_format", "yml")
    i18n.set("locale", "it")
    i18n.set("enable_memoization", True)
    i18n.load_path.append("bot/messages")

    bot = FlutterItaliaBot(token=os.environ.get("TOKEN", ""))
    bot.setup()
    bot.run()


main()
